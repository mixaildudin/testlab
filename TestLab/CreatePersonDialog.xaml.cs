﻿using System.Windows;
using TestLab.Data.Models;

namespace TestLab
{
    public partial class CreatePersonDialog : Window
    {
        public CreatePersonDialog()
        {
            InitializeComponent();
        }

        public string FirstName => FirstNameTextBox.Text;

        public string LastName => LastNameTextBox.Text;

        public Sex Sex => MaleRadioButton.IsChecked == true ? Sex.Male : Sex.Female;

        public int Age => int.Parse(AgeTextBox.Text);

        public string CityName => CityTextBox.Text;

        private void OnOkButtonClicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void OnCancelClicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
