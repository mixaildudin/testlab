﻿using System;
using System.Linq;
using TestLab.Data.Models;

namespace TestLab.Data
{
    public class StatsHelper
    {
        private readonly PeopleRepository _repository;

        public StatsHelper(PeopleRepository repository)
        {
            _repository = repository;
        }

        public int GetTotalCount()
        {
            return _repository.GetAllPeople().Count;
        }

        public int GetPeopleCountForSex(Sex s)
        {
           return _repository
                .GetAllPeople()
                .Count(x => x.Sex == s);
        }

        public int GetMaxAge()
        {
            return _repository.GetAllPeople().Select(x => x.Age).DefaultIfEmpty().Max();
        }

        public int GetMinAge()
        {
            return _repository.GetAllPeople().Select(x => x.Age).DefaultIfEmpty().Min();
        }

        public string GetMostFrequentCity()
        {
            var result = _repository
                .GetAllPeople()
                .GroupBy(x => x.CityName)
                .Select(g => new { City = g.Key, Count = g.Count() })
                .OrderByDescending(x => x.Count)
                .FirstOrDefault();

            return result?.City;
        }

        public double GetAverageAge()
        {
            var avg = _repository
                .GetAllPeople()
                .Select(x => x.Age)
                .DefaultIfEmpty()
                .Average();

            return Math.Round(avg, 2, MidpointRounding.AwayFromZero);
        }
    }
}
