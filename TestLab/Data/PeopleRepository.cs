﻿using System.Collections.Generic;
using TestLab.Data.Models;

namespace TestLab.Data
{
    public class PeopleRepository
    {
        private readonly List<Person> _people = new List<Person>();

        public List<Person> GetAllPeople() => _people;

        public void AddPerson(Person p)
        {
            _people.Add(p);
        }
    }
}
