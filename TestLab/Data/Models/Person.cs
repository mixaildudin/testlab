﻿namespace TestLab.Data.Models
{
    public class Person
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Sex Sex { get; set; }

        public int Age { get; set; }

        public string CityName { get; set; }
    }
}
