﻿using System.Windows;
using TestLab.Data;

namespace TestLab
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var repository = new PeopleRepository();
            var statsHelper = new StatsHelper(repository);

            new MainWindow(repository, statsHelper).Show();
        }
    }
}
