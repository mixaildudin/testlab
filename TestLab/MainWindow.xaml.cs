﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using TestLab.Annotations;
using TestLab.Data;
using TestLab.Data.Models;

namespace TestLab
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private readonly PeopleRepository _repository;

        private readonly StatsHelper _statsHelper;
        
        public MainWindow(PeopleRepository repository, StatsHelper statsHelper)
        {
            _repository = repository;
            _statsHelper = statsHelper;
            InitializeComponent();
            this.DataContext = this;
        }

        private List<Person> _peopleList;

        public List<Person> People => 
            new List<Person>(_peopleList ?? new List<Person>());

        private int _maxAge;

        public int MaxAge
        {
            get { return _maxAge; }
            set
            {
                _maxAge = value;
                OnPropertyChanged(nameof(MaxAge));
            }
        }

        private int _totalCount;

        public int TotalCount
        {
            get { return _totalCount; }
            set
            {
                _totalCount = value;
                OnPropertyChanged(nameof(TotalCount));
            }
        }

        private int _minAge;

        public int MinAge
        {
            get { return _minAge; }
            set
            {
                _minAge = value;
                OnPropertyChanged(nameof(MinAge));
            }
        }

        private int _menCount;

        public int MenCount
        {
            get { return _menCount; }
            set
            {
                _menCount = value;
                OnPropertyChanged(nameof(MenCount));
            }
        }

        private int _womenCount;

        public int WomenCount
        {
            get { return _womenCount; }
            set
            {
                _womenCount = value;
                OnPropertyChanged(nameof(WomenCount));
            }
        }

        private double _avgAge;

        public double AvgAge
        {
            get { return _avgAge; }
            set
            {
                _avgAge = value;
                OnPropertyChanged(nameof(AvgAge));
            }
        }

        private string _mostFrequentCity;

        public string MostFrequentCity
        {
            get { return _mostFrequentCity; }
            set
            {
                _mostFrequentCity = value;
                OnPropertyChanged(nameof(MostFrequentCity));
            }
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
            var createDialog = new CreatePersonDialog();
            if (createDialog.ShowDialog() == false)
            {
                return;
            }

            var person = new Person
            {
                FirstName = createDialog.FirstName,
                LastName = createDialog.LastName,
                Age = createDialog.Age,
                Sex = createDialog.Sex,
                CityName = createDialog.CityName
            };

            _repository.AddPerson(person);

            UpdatePeopleList();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UpdatePeopleList()
        {
            _peopleList = _repository.GetAllPeople();
            OnPropertyChanged(nameof(People));

            UpdateStats();
        }

        private void UpdateStats()
        {
            TotalCount = _statsHelper.GetTotalCount();
            MenCount = _statsHelper.GetPeopleCountForSex(Sex.Male);
            WomenCount = _statsHelper.GetPeopleCountForSex(Sex.Female);
            MaxAge = _statsHelper.GetMaxAge();
            MinAge = _statsHelper.GetMinAge();
            AvgAge = _statsHelper.GetAverageAge();
            MostFrequentCity = _statsHelper.GetMostFrequentCity();
        }

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            UpdatePeopleList();
        }
    }
}
