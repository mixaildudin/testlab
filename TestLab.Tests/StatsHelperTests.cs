﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TestLab.Data;
using TestLab.Data.Models;

namespace TestLab.Tests
{
    [TestFixture]
    public class StatsHelperTests
    {
        private PeopleRepository _repository;

        private StatsHelper _statsHelper;

        [SetUp]
        public void SetUp()
        {
            _repository = new PeopleRepository();
            _statsHelper = new StatsHelper(_repository);
        }

        private static IEnumerable<TestCaseData> AvgAgeTestData
        {
            get
            {
                yield return new TestCaseData(new[] { 15, 16, 17 }).Returns(16);
                yield return new TestCaseData(new[] { 0, 0, 0 }).Returns(0);
                yield return new TestCaseData(new[] { 15, 20 }).Returns(17.5);
                yield return new TestCaseData(new[] { 100, 201, 300 }).Returns(200.33);
            }
        }

        [Test, TestCaseSource(nameof(AvgAgeTestData))]
        public double StatsHelper_AgesProvided_CorrectAvgValuesCalculated(int[] ages)
        {
            var people = ages.Select(age => new Person { Age = age });
            foreach (var person in people)
            {
                _repository.AddPerson(person);
            }

            return _statsHelper.GetAverageAge();
        }

        private static IEnumerable<TestCaseData> MaxAgeTestData
        {
            get
            {
                yield return new TestCaseData(new[] { 20, 21, 19, 22, 18, 17 }).Returns(22);
                yield return new TestCaseData(new[] { 0, 15 }).Returns(15);
                yield return new TestCaseData(new[] { -15, 15 }).Returns(15);
                yield return new TestCaseData(new[] { int.MinValue, int.MaxValue }).Returns(int.MaxValue);
            }
        }

        [Test, TestCaseSource(nameof(MaxAgeTestData))]
        public int StatsHelper_AgesProvided_CorrectMaxAgeCalculated(int[] ages)
        {
            foreach (var person in ages.Select(age => new Person { Age = age }))
            {
                _repository.AddPerson(person);
            }

            return _statsHelper.GetMaxAge();
        }

        [TestCase(new[] { 35, 32, 46, 21, 62 }, ExpectedResult = 21)]
        [TestCase(new[] { int.MinValue, 0, int.MaxValue }, ExpectedResult = int.MinValue)]
        [TestCase(new[] { 20, 20 }, ExpectedResult = 20)]
        public int StatsHelper_AgesProvided_CorrectMinAgeCalculated(int[] ages)
        {
            foreach (var person in ages.Select(age => new Person { Age = age }))
            {
                _repository.AddPerson(person);
            }

            return _statsHelper.GetMinAge();
        }
        
        [TestCase(Sex.Male, 0)]
        [TestCase(Sex.Male, 150)]
        [TestCase(Sex.Female, 0)]
        [TestCase(Sex.Female, 150)]
        public void StatsHelper_MultiplePeopleOfDifferentSex_CountBySexCalculatedCorrectly(Sex sex, int count)
        {
            foreach (var person in Enumerable.Range(0, count).Select(x => new Person {Sex = sex}))
            {
                _repository.AddPerson(person);
            }

            var calculatedCount = _statsHelper.GetPeopleCountForSex(sex);

            Assert.AreEqual(count, calculatedCount);
        }

        private static IEnumerable<TestCaseData> MostFrequentCityTestData
        {
            get
            {
                yield return new TestCaseData(new[] { new[] { "a", "b", "a", "c", "a", "b" } }).Returns("a");
                yield return new TestCaseData(new[] { new[] { "a", "b" } }).Returns("a");
                yield return new TestCaseData(new[] { new [] {"a", "a"} }).Returns("a");
                yield return new TestCaseData(new[] { new [] { "a" } }).Returns("a");
            }
        }

        [Test, TestCaseSource(nameof(MostFrequentCityTestData))]
        public string StatsHelper_PeopleWithCities_MostFrequentCityCalculated(string[] cities)
        {
            foreach (var person in cities.Select(x => new Person { CityName = x}))
            {
                _repository.AddPerson(person);
            }

            return _statsHelper.GetMostFrequentCity();
        }
    }
}
