# Как запустить проект #

* К сожалению, сначала поставить триал [Visual Studio Enterpsise](https://www.visualstudio.com/ru/downloads/)
* Открыть в студии .sln-файл
* Зеленая кнопка Run

# Как запустить юнит-тесты #
* Т.к. нативной поддержки NUnit в студии нет, нужно поставить маленькое расширение.
В студии Tools -> Extensions and Updates -> поискать по запросу "nunit", установить NUnit3 Test Adapter
* Открыть файл StatsHelperTests.cs, правой кнопкой в редакторе кода, Run Tests

# Как запустить тесты UI #
* Перед первым запуском побилдить сам проект TestLab. После этого в Solution Explorer в проекте UiTests нужно найти файл UIMap.uitests, дважды по нему кликнуть. Выбрать метод RunApp, выбрать там действие Launch..., правой кнопкой на него, Properties. Либо в поле File Name, либо Altername File Name указать путь до собранного ехе-файла TestLab. Все это делается, чтобы студия точно нашла нужный экзешник.
* Открыть файл UserInterfaceTests.cs, правой кнопкой в редакторе кода, Run Tests